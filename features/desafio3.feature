Feature: Desafio 3
    @contract
    Scenario: Validar que o número de filmes do 'Luke Skywalker' é 5

        * def swapiPeople = read('../data/swapiPeople.json')

        # jsonpath filtering
        #ref1: https://github.com/json-path/JsonPath#filter-operators
        #ref2: https://intuit.github.io/karate/#json-transforms (um pouco pra cima)

        # karate get
        #ref1: https://intuit.github.io/karate/#get

        * def luke = get[0] swapiPeople $.results[?(@.name=='Luke Skywalker')]
        * print luke

        # karate expressions / karate object
        * def lukeFilmsSize = karate.sizeOf(luke.films)

        # karate print
        * print lukeFilmsSize
