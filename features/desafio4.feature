Feature: Desafio 4
    @contract
    Scenario: Validar que 'Biggs Darklighter' está na lista de pessoas (people)

        * def swapiPeople = read('../data/swapiPeople.json')

        # matching json arrays (Matching Sub-Sets of JSON Keys and Arrays > json arrays)
        # karate match contains
        Then match swapiPeople.results..name contains 'Biggs Darklighter'
        Then match swapiPeople.results[*].name contains 'Biggs Darklighter'