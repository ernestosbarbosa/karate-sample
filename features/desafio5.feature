Feature: Desafio 5
    @acceptance
    Scenario: Validar que cada personagem da lista de pessoas possui pelo menos um filme

        * def swapiPeople = read('../data/swapiPeople.json')

        #karate schema validation
        Then match each swapiPeople.results[*].films == '#[_ > 0] #string'
