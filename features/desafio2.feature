Feature: Desafio 2

Background:
    * def c = karate.read('../utils/karate-call.js')
    * def v = karate.read('../utils/karate-validate.js')
    * def PAGE = (typeof(__arg) != "undefined") ? __arg.PAGE : 1
    * def COUNT = (typeof(__arg) != "undefined") ? __arg.COUNT : 0
    * def TOTAL = (typeof(__arg) != "undefined") ? __arg.TOTAL : 0

Scenario: Validação de quantidade ou chamada recursiva para as páginas
    Given url 'https://swapi.co/api/people/?page='+PAGE
    When method GET
    * print COUNT
    * print TOTAL
    And eval if (responseStatus == 200) c(PAGE,response.count,COUNT,karate.sizeOf(response.results))
    Then assert responseStatus == 404 ? v(TOTAL,COUNT) : true
