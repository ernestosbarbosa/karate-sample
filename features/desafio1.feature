Feature: Desafio 1
    @health_check
    Scenario Outline: Validar que os endpoints: /people, /planets e /starships retornam status de sucesso

        # karate url / path / method HTTP / status HTTP
        # karate scenario outline w/ examples

        * url "https://swapi.co/api/"
        Given path '<endpoint>'
        When method GET
        Then status 200

        Examples:
            | endpoint  |
            | people    |
            | planets   |
            | starships |
